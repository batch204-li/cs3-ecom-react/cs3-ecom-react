
import {Link, NavLink,} from 'react-router-dom';
import {Button} from 'react-bootstrap';


import {Container, Nav, Navbar, NavDropdown} from 'react-bootstrap';

import {useContext} from 'react'; 
import UserContext from '../UserContext';

export default function AppNavBar() 
{
	const {user} = useContext(UserContext);

	return (
		<Navbar bg="dark" variant="dark" expand="lg">
		     <Container>
		       <Navbar.Brand as = {Link} to= "/" >Air Jordan</Navbar.Brand>
		       <Navbar.Toggle aria-controls="basic-navbar-nav" />
		       <Navbar.Collapse id="basic-navbar-nav">
		         

		         <Nav className="ms-auto">
		         	
		         		
	         		<Nav.Link as = {NavLink}  to = "/">Home</Nav.Link>
	         		<Nav.Link as = {NavLink} to = "/courses">Products</Nav.Link>
	         		
	         		
	         		{(user.id !== null) ?
	         			<>
	         			<Link className= "nav-link" to="/logout"> Logout</Link>

	         			<Button variant="outline-info" disabled>
	         			       {`${user.email}`}
	         			     </Button>
	         			</>

	         			:
	         			<>
	         			<Nav.Link as = {NavLink} to = "/login">Login</Nav.Link>
	         			<Nav.Link as = {NavLink} to = "/register">Register</Nav.Link>
	         			</>
	         		}
		         	
		         </Nav>
		       </Navbar.Collapse>
		     </Container>
		   </Navbar>	
	);
}