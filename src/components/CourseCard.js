import {Col, Card, Button} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import {Link} from 'react-router-dom'

export default function CourseCard ({courseProp})
{
	
	const {_id, name, description, price} = courseProp;
	

	return (
			<Col>
				<Card className = "courseCard my-2">
					<Card.Body>
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle className = "mt-3"> Description: </Card.Subtitle>
						<Card.Text>{description}</Card.Text>

						<Card.Subtitle className = "mt-3">Price:</Card.Subtitle>
						<Card.Text>PhP {price}</Card.Text>
						
						
       					 <Link className= "btn btn-primary" to= {`courses/${_id}`} >Details</Link>
					</Card.Body>
					
					
				</Card>

			</Col>
		)
}