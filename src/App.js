
import './App.css';
import AppNavBar from './components/AppNavBar';
import Courses from './pages/Courses' ;
import SpecificProduct from './pages/SpecificProduct'
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';

import {UserProvider} from './UserContext';

import {useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'



function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
    email: null
  })

  useEffect (() => 
  {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`,
    {
      method: "POST",
      headers: 
      {
        Authorization : `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then (res => res.json())
    .then ( data => {
      // setUser to these values
      setUser (
      {
        id: data._id,
        isAdmin: data.isAdmin,
        email: data.email
      })
    })
  }, [])


  return (

    <UserProvider value={{user, setUser}} >
      <Router>
        <>
        <AppNavBar />
        <Container>
            <Switch>
              <Route exact path = "/" component = {Home}/>
              <Route exact path = "/register" component = {Register}/>
              <Route exact path = "/login" component = {Login}/>
              <Route exact path = "/logout" component = {Logout}/>
              <Route exact path = "/courses" component = {Courses}/>
              <Route exact path = "/courses/:courseId" component = {SpecificProduct}/>
              <Route component = {Error}/>
            </Switch>  
        </Container> 
        </>    
      </Router>
    </UserProvider>
  );
}

export default App;
