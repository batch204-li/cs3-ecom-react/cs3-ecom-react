// IMPORT 
	import {useEffect, useState, useContext} from 'react';
	//import courseData from '../data/courseData' ;
	import CourseCard from '../components/CourseCard';
	import AdminView from '../components/AdminView';
	import UserContext from '../UserContext'
	


	export default function Courses()
	{
		const [coursesData, setCoursesData] = useState ([]);

		const {user} = useContext(UserContext)
		

		const fetchData = () => 
		{
			fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
			.then (res => res.json())
			.then (data => 
			{
				console.log(data)
				setCoursesData(data)
			})
		}

		
		useEffect(() =>
		{
			console.log(process.env.REACT_APP_API_URL)
			
			fetchData()
			
		}, [])

		const courses = coursesData.map(course =>
		{
			if (course.isActive)
			{
				return (
					<CourseCard courseProp = {course} key={course._id} />
					)
			}
			else
			{
				return null
			}
		})

		
		return ( (user.isAdmin) ?
			<AdminView coursesProp={coursesData} fetchData={fetchData} />
			:
			<>
				<h1>Courses</h1>
				{courses}
				
			</>
				
			)
	}

	