import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home ()
{
	const data = 
	{
		title: "Welcome to Air Jordan",
		content: "The key to success is failure.",
		destination: "/courses",
		label: "Browse Shoes"
	}

	return (
		<>
			<Banner dataProp={data} />
		    <Highlights />
		    
		</>
		)
}